// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require turbolinks
//= require_tree .
//
//
$.ajaxSetup({
  dataType: "json",
  type: "POST",
  statusCode:{
    422: function(response)
    {
      RenderError(response.responseText);
    }
  }
});

function RenderError(error)
{
  $("#notices").html('<div class="alert alert-danger">'+error+'</div>');
  ScrollToNotice();
}

function RenderNotice(notice)
{
  $("#notices").html('<div class="alert alert-success">'+notice+'</div>');
  ScrollToNotice();
}

$(document).on("click","#find_friend", function(e){
  e.preventDefault();
  searched_email = $("#search_friend_input").val();
  alert(searched_email);
  $.ajax({
    url: "/friends",
    data: {email: searched_email},
    statusCode:{
      201: function(response)
      {
        RenderNotice("Invite has been send");
      }
    }
  })
});

$(document).on("click",".remove_product", function(e){
  e.preventDefault();
  var sel = $(this).parent().parent();
  var id = $(this).data("id");
  $.ajax({
    url: "/products/"+id,
    data: {
           "_method": "DELETE"
    },
    statusCode:{
      200: function(response)
      {
        sel.remove();
      }
    }
  });
});

$(document).on("click",".bought_product", function(e){
  e.preventDefault();
  var sel = $(this).parent().parent();
  var id = $(this).data("id");
  var container = $("#boughts");
  $.ajax({
    url: "/products/"+id+"/bought",
    data: {
           "_method": "put"
    },
    statusCode:{
      200: function(response)
      {
        sel.remove();
        object = response;
        container.html(response.responseText);
      }
    }
  });
});

