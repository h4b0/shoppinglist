class List < ActiveRecord::Base
  has_many :products
  belongs_to :user

  def close
    update(status: "closed")
  end

  def closed?
    status == "closed"
  end
end
