class Product < ActiveRecord::Base
  belongs_to :list
  belongs_to :user
  delegate :email, to: :user, prefix: true 
  scope :bought, ->{where(status: "bought")}
  scope :fresh, ->{where(status: "new")}
end
