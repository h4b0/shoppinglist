class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.string :name, null: false
      t.string :status, default: "new"
      t.integer :user_id
      t.timestamps
    end
  end
end
