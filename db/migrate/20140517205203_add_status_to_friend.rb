class AddStatusToFriend < ActiveRecord::Migration
  def change
    add_column :friends, :status, :string, default: "new"
  end
end
