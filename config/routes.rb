ShoppingList::Application.routes.draw do
  resources :lists do
    member do
      post :add_product
      put :close
    end
  end

  resources :products do
    member do
      put :bought
    end
  end

  devise_for :users
  root to: "lists#index"
end
