# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
ShoppingList::Application.config.secret_key_base = 'b7deec173680d3dcf207ea4754ebd045d5dc11b764bf80c13415508084a629141c0169f95f324ebf1c91c445f854fcc88304e7305ef428a0359db0b014966b31'
